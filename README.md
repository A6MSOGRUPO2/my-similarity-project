[![build status](https://gitlab.com/A6MSO/my-similarity-project/badges/master/build.svg)](https://gitlab.com/A6MSO/my-similarity-project/commits/master)


[![coverage report](https://gitlab.com/A6MSO/my-similarity-project/badges/master/coverage.svg)](https://gitlab.com/A6MSO/my-similarity-project/commits/master)


Boa tarde a todos,
Como todos já sabem fizemos a migração para desenvolvimento com branches para minimizar as perdas de código e para nos adequarmos ao processo de desenvolvimento de entrega contínua (Continuous Delivery - CD) .
Com o objetivo de continuar com o processo de desenvolvimento pessoal, aprimoramento e aprendizagem, seguem cursos em vídeos sobre o tema e documentação para disseminar o conhecimento.
 
Vídeos de Treinamentos de Git e documentação do Git e artigos sobre Git Flow.
Os treinamentos são em vídeo para facilitar a compreensão e aprendizado.
 
Básico :
Necessário realizar cadastro no site.
http://willianjusten.teachable.com/courses/enrolled/61310
 
Git no Plugin do Eclipse:
Vídeos sobre utilização no plugin do Git no Eclipse, resolução de conflitos entre outros.
https://www.youtube.com/watch?v=kmfIRJWp6tQ&list=PLBbgqtDgdc_TK29Bt88XaDBcQfzer6L2a
 
Fluxo de Git:
https://about.gitlab.com/2016/07/27/the-11-rules-of-gitlab-flow/
https://about.gitlab.com/2014/09/29/gitlab-flow/
http://nvie.com/posts/a-successful-git-branching-model/
 
Documentação Completa oficial Git
Versão em português 2009 : https://git-scm.com/book/pt-br/v1
Última versão em inglês 2014: https://progit2.s3.amazonaws.com/en/2016-03-22-f3531/progit-en.1084.pdf


RUP http://www.funpar.ufpr.br:8080/rup/process/ovu_proc.htm